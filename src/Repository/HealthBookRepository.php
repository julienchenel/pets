<?php

namespace App\Repository;

use App\Entity\HealthBook;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HealthBook|null find($id, $lockMode = null, $lockVersion = null)
 * @method HealthBook|null findOneBy(array $criteria, array $orderBy = null)
 * @method HealthBook[]    findAll()
 * @method HealthBook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HealthBookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HealthBook::class);
    }

    // /**
    //  * @return HealthBook[] Returns an array of HealthBook objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HealthBook
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
