<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Entity\HealthBook;
use App\Form\HealthBookType;
use App\Repository\HealthBookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/health/book")
 */
class HealthBookController extends AbstractController
{
    /**
     * @Route("/", name="health_book_index", methods={"GET"})
     */
    public function index(HealthBookRepository $healthBookRepository): Response
    {
        return $this->render('health_book/index.html.twig', [
            'health_books' => $healthBookRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="health_book_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $healthBook = new HealthBook();
        $form = $this->createForm(HealthBookType::class, $healthBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($healthBook);
            $entityManager->flush();

            return $this->redirectToRoute('health_book_index');
        }

        return $this->render('health_book/new.html.twig', [
            'health_book' => $healthBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="health_book_show", methods={"GET"})
     */
    public function show(HealthBook $healthBook): Response
    {
        return $this->render('health_book/show.html.twig', [
            'health_book' => $healthBook,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="health_book_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HealthBook $healthBook): Response
    {
        $form = $this->createForm(HealthBookType::class, $healthBook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('health_book_index');
        }

        return $this->render('health_book/edit.html.twig', [
            'health_book' => $healthBook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="health_book_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HealthBook $healthBook): Response
    {
        if ($this->isCsrfTokenValid('delete'.$healthBook->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($healthBook);
            $entityManager->flush();
        }

        return $this->redirectToRoute('health_book_index');
    }
}
