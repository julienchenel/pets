<?php

namespace App\Controller;

use App\Entity\SportActivities;
use App\Form\SportActivitiesType;
use App\Repository\SportActivitiesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sport/activities")
 */
class SportActivitiesController extends AbstractController
{
    /**
     * @Route("/", name="sport_activities_index", methods={"GET"})
     */
    public function index(SportActivitiesRepository $sportActivitiesRepository): Response
    {
        return $this->render('sport_activities/index.html.twig', [
            'sport_activities' => $sportActivitiesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sport_activities_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sportActivity = new SportActivities();
        $form = $this->createForm(SportActivitiesType::class, $sportActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sportActivity);
            $entityManager->flush();

            return $this->redirectToRoute('sport_activities_index');
        }

        return $this->render('sport_activities/new.html.twig', [
            'sport_activity' => $sportActivity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sport_activities_show", methods={"GET"})
     */
    public function show(SportActivities $sportActivity): Response
    {
        return $this->render('sport_activities/show.html.twig', [
            'sport_activity' => $sportActivity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sport_activities_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SportActivities $sportActivity): Response
    {
        $form = $this->createForm(SportActivitiesType::class, $sportActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sport_activities_index');
        }

        return $this->render('sport_activities/edit.html.twig', [
            'sport_activity' => $sportActivity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sport_activities_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SportActivities $sportActivity): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sportActivity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sportActivity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sport_activities_index');
    }
}
