<?php

namespace App\Controller;

use App\Entity\Nutrition;
use App\Form\NutritionType;
use App\Repository\NutritionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nutrition")
 */
class NutritionController extends AbstractController
{
    /**
     * @Route("/", name="nutrition_index", methods={"GET"})
     */
    public function index(NutritionRepository $nutritionRepository): Response
    {
        return $this->render('nutrition/index.html.twig', [
            'nutrition' => $nutritionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="nutrition_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $nutrition = new Nutrition();
        $form = $this->createForm(NutritionType::class, $nutrition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($nutrition);
            $entityManager->flush();

            return $this->redirectToRoute('nutrition_index');
        }

        return $this->render('nutrition/new.html.twig', [
            'nutrition' => $nutrition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nutrition_show", methods={"GET"})
     */
    public function show(Nutrition $nutrition): Response
    {
        return $this->render('nutrition/show.html.twig', [
            'nutrition' => $nutrition,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="nutrition_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Nutrition $nutrition): Response
    {
        $form = $this->createForm(NutritionType::class, $nutrition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nutrition_index');
        }

        return $this->render('nutrition/edit.html.twig', [
            'nutrition' => $nutrition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nutrition_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Nutrition $nutrition): Response
    {
        if ($this->isCsrfTokenValid('delete'.$nutrition->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($nutrition);
            $entityManager->flush();
        }

        return $this->redirectToRoute('nutrition_index');
    }
}
