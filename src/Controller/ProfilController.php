<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Animal;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfilController extends AbstractController
{
    
    /**
     * @Route("/profil", name="user_profil")
     */
    public function profil()
    {
        //verifie si user est connecté
        $this->denyAccessUnlessGranted('ROLE_USER');
        //stock dans la variable $user l'id de l'utilisateur courrent
        $user = $this->getUser()->getId();
        //on recupere dans $monuser l'objet utilsateur via l'id
        $monuser = $this->getDoctrine()->getRepository(User::class)->find($user);
        //on recupere dans $monanimal l'objet animal via l'id de l'utilisateur
        $monanimal= $this->getDoctrine()->getRepository(Animal::class)->findBy(['user'=>$monuser]);

        return $this->render('user/profil.html.twig', [
            'user' => $monuser,
            'animal' => $monanimal
        ]);
    }
}
