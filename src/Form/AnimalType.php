<?php

namespace App\Form;

use App\Entity\Animal;
use App\Entity\HealthBook;
use App\Entity\Nutrition;
use App\Entity\SportActivities;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('birthday')
            ->add('identificationNumber')
            ->add('pedigree')
            ->add('idPhotoPath')
            ->add('type')
            ->add('healthBook', HealthBookType::class)
            ->add('nutrition', NutritionType::class)
            ->add('sportActivities', SportActivitiesType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Animal::class,
        ]);
    }
}
