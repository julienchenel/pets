<?php

namespace App\Form;

use App\Entity\HealthBook;
use App\Entity\Animal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HealthBookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('veterinarian')
            ->add('diseases')
            ->add('vaccines')
            ->add('antiparasites')
            ->add('weight')
            ->add('surgeries')
            ->add('veterinarianComments')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HealthBook::class,
        ]);
    }
}
