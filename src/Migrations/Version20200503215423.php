<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200503215423 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE health_book (id INT AUTO_INCREMENT NOT NULL, animal_id INT NOT NULL, veterinarian VARCHAR(255) NOT NULL, diseases VARCHAR(255) DEFAULT NULL, vaccines VARCHAR(255) DEFAULT NULL, antiparasites VARCHAR(255) DEFAULT NULL, weight VARCHAR(255) NOT NULL, surgeries VARCHAR(255) DEFAULT NULL, veterinarian_comments VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_D660748E8E962C16 (animal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE health_book ADD CONSTRAINT FK_D660748E8E962C16 FOREIGN KEY (animal_id) REFERENCES animal (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE health_book');
    }
}
