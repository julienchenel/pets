<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NutritionRepository")
 */
class Nutrition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dietFood;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Animal", inversedBy="nutrition", cascade={"persist", "remove"})
     */
    private $animal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDietFood(): ?string
    {
        return $this->dietFood;
    }

    public function setDietFood(?string $dietFood): self
    {
        $this->dietFood = $dietFood;

        return $this;
    }

    public function getAnimal(): ?Animal
    {
        return $this->animal;
    }

    public function setAnimal(?Animal $animal): self
    {
        $this->animal = $animal;

        return $this;
    }
}
