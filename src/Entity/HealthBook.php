<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HealthBookRepository")
 */
class HealthBook
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $veterinarian;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diseases;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vaccines;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $antiparasites;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surgeries;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $veterinarianComments;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Animal", inversedBy="healthBook", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $animal;

    public function __toString() {
        return $this->veterinarian;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVeterinarian(): ?string
    {
        return $this->veterinarian;
    }

    public function setVeterinarian(string $veterinarian): self
    {
        $this->veterinarian = $veterinarian;

        return $this;
    }

    public function getDiseases(): ?string
    {
        return $this->diseases;
    }

    public function setDiseases(?string $diseases): self
    {
        $this->diseases = $diseases;

        return $this;
    }

    public function getVaccines(): ?string
    {
        return $this->vaccines;
    }

    public function setVaccines(?string $vaccines): self
    {
        $this->vaccines = $vaccines;

        return $this;
    }

    public function getAntiparasites(): ?string
    {
        return $this->antiparasites;
    }

    public function setAntiparasites(?string $antiparasites): self
    {
        $this->antiparasites = $antiparasites;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSurgeries(): ?string
    {
        return $this->surgeries;
    }

    public function setSurgeries(?string $surgeries): self
    {
        $this->surgeries = $surgeries;

        return $this;
    }

    public function getVeterinarianComments(): ?string
    {
        return $this->veterinarianComments;
    }

    public function setVeterinarianComments(?string $veterinarianComments): self
    {
        $this->veterinarianComments = $veterinarianComments;

        return $this;
    }

    public function getAnimal(): ?Animal
    {
        return $this->animal;
    }

    public function setAnimal(Animal $animal): self
    {
        $this->animal = $animal;

        return $this;
    }
}
