<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SportActivitiesRepository")
 */
class SportActivities
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sportType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sportingObjective;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Animal", inversedBy="sportActivities", cascade={"persist", "remove"})
     */
    private $animal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSportType(): ?string
    {
        return $this->sportType;
    }

    public function setSportType(?string $sportType): self
    {
        $this->sportType = $sportType;

        return $this;
    }

    public function getSportingObjective(): ?string
    {
        return $this->sportingObjective;
    }

    public function setSportingObjective(?string $sportingObjective): self
    {
        $this->sportingObjective = $sportingObjective;

        return $this;
    }

    public function getAnimal(): ?Animal
    {
        return $this->animal;
    }

    public function setAnimal(?Animal $animal): self
    {
        $this->animal = $animal;

        return $this;
    }
}
