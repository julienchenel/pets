<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 */
class Animal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identificationNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pedigree;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idPhotoPath;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\HealthBook", mappedBy="animal", cascade={"persist", "remove"})
     * @Assert\Type(type="App\Entity\HealthBook")
     * @Assert\Valid
     */
    private $healthBook;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SportActivities", mappedBy="animal", cascade={"persist", "remove"})
     */
    private $sportActivities;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Nutrition", mappedBy="animal", cascade={"persist", "remove"})
     */
    private $nutrition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="animals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __toString() {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(string $identificationNumber): self
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    public function getPedigree(): ?string
    {
        return $this->pedigree;
    }

    public function setPedigree(string $pedigree): self
    {
        $this->pedigree = $pedigree;

        return $this;
    }

    public function getIdPhotoPath(): ?string
    {
        return $this->idPhotoPath;
    }

    public function setIdPhotoPath(?string $idPhotoPath): self
    {
        $this->idPhotoPath = $idPhotoPath;

        return $this;
    }

    public function getHealthBook(): ?HealthBook
    {
        return $this->healthBook;
    }

    public function setHealthBook(HealthBook $healthBook): self
    {
        $this->healthBook = $healthBook;

        // set the owning side of the relation if necessary
        if ($healthBook->getAnimal() !== $this) {
            $healthBook->setAnimal($this);
        }

        return $this;
    }

    public function getSportActivities(): ?SportActivities
    {
        return $this->sportActivities;
    }

    public function setSportActivities(?SportActivities $sportActivities): self
    {
        $this->sportActivities = $sportActivities;

        // set (or unset) the owning side of the relation if necessary
        $newAnimal = null === $sportActivities ? null : $this;
        if ($sportActivities->getAnimal() !== $newAnimal) {
            $sportActivities->setAnimal($newAnimal);
        }

        return $this;
    }

    public function getNutrition(): ?Nutrition
    {
        return $this->nutrition;
    }

    public function setNutrition(?Nutrition $nutrition): self
    {
        $this->nutrition = $nutrition;

        // set (or unset) the owning side of the relation if necessary
        $newAnimal = null === $nutrition ? null : $this;
        if ($nutrition->getAnimal() !== $newAnimal) {
            $nutrition->setAnimal($newAnimal);
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
